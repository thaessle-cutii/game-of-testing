# Game of testing

TDD path to Throne

## Project's Anatomy
We will create "Game of Throne" simulator.<br/>
In the MVP Project we have 6 Houses : Stark, Lannister, Martell, Targaryen, Baratheon, Greyjoy.

Our first functionality is to manage a born.<br/>
A new born have a name, a house, age of 0, no titles, no profession, no skills, no allies, no ennemies.
From this moment, he become an inhabitant of Westeros.

Then he may get old and from 8 years old he get a profession : 'courtier', 'knight', 'spy', 'maester' or 'priest'.

All along his short life he can get titles, skills, allies or ennemies.

Titles are short description which act of glory or infamy.

Peoples have 5 skills : 'knowledge', 'faith', 'strength', 'agility', 'treachery'. Their levels are numbers between 0 and 10.

When his house sign an alliance with another, all members of the other become allies.  <br/>
When his house go in war with another, all members of the the other become ennemies.<br/>
Alliance and War are not transitive : negociations are more subtles.<br/>
A people have to get the title 'Lord of House' to declare war to another.<br/>

Sometime we discover unkonwed peoples so we need to register them.

## Hacker stuff
You will implement People management and war/peace declaration.<br/>
We will do this in node.js with some tools :
* to properly code in ES6 we will use the linter [eslint](http://eslint.org/)
  * All is preconfigure for this project.
    * The file [.eslintrc](./.eslintrc) define rules applied
    * The file [.eslintignore](./.eslintignore) define pattern for files to exclude from the linter
  * You can run `npm run lint` to run the linter
  * You can run `npm run lint:fix` to fix automatically fixable errors (scripts are define in [package.json](./package.json))
* We will use [mocha](https://mochajs.org/) as a test runner
  * You can run `npm run test` to launch tests
* We will use [chai](http://chaijs.com/) as an assertion engine
* We will produce Istanbul analysis with [nyc](https://istanbul.js.org)

All is setup in devDependencies. Run `npm i` before stat to code.

## Go to code ?
Not yet ! First we will describe a naive approach of this game. <br/>
We will have a Model "people" that describe our object or type (there is no declarative Types in ES6, you need to use Typescript or Flow to do it ... in our trial we will use a class) and a Controller "peopleContoler" that describe available actions.<br/>
BUT before coding we will describe our stories.

### Create the Model
Go to ./models/people.test.js to complete the specification of the model.

Run your tests with `npm run test` ... it will fail ! which is good because you don't implement them.

Implement your model and run tests until it success.


### Create the Controller
Go to ./controllers/people.test.js and imagine the specification of the controller.
At least you should be able to 'born', 'kill', 'register' or 'train' peoples.
They also must be able to get a job, allies, ennemies and titles.
Finaly they may declare war or sign peace with another house.

Run your tests with `npm run test` ... it will fail ! which is good because you don't implement them.

Implement your model and run tests until it success.
